import images as im


def sepia(image: dict) -> dict:
    width, height = im.size(image)
    new_image = im.create_blank(width, height)

    for i in range(width * height):
        pixel = image['pixels'][i]
        original_r = pixel[0]
        original_g = pixel[1]
        original_b = pixel[2]
        sepia_r = round(0.393 * original_r + 0.769 * original_g + 0.189 * original_b)
        sepia_g = round(0.349 * original_r + 0.686 * original_g + 0.168 * original_b)
        sepia_b = round(0.272 * original_r + 0.534 * original_g + 0.131 * original_b)
        pixel_sepia = (sepia_r, sepia_g, sepia_b)
        pixel_sepia_lista = list(pixel_sepia)
        for x in range(len(pixel_sepia)):
            if pixel_sepia_lista[x] < 0:
                pixel_sepia_lista[x] = 0
            elif pixel_sepia_lista[x] > 255:
                pixel_sepia_lista[x] = 255
        new_image['pixels'][i] = pixel_sepia

    im.write_img(image=new_image, filename='imagen_sepia.png')
    return new_image

pixels = im.read_img("cafe.jpg")
sepia(pixels)