import images

"""Rectángulo de tres por dos pixels, con los colores rojo (los dos pixels de arriba)
y azul (los dos pixels de abajo)
"""
imagen = {'width': 3,
          'height': 2,
          'pixels': [(255, 0, 0), (255, 0, 0), (255, 0, 0),
                     (0, 0, 255), (0, 0, 255), (0, 0, 255)]
          }
images.write_img(image=imagen, filename='imagen_ejemplo.png')