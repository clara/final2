# ENTREGA CONVOCATORIA JUNIO
Clara García Peña -- c.garciape.2023@alumnos.urjc.es
- VIDEO DEMOSTRACIÓN: https://youtu.be/BquzUAiZmHE

- REQUISITOS MÍNIMOS: 
  - Método "mirror"
  - Método "grayscale"
  - Método "blur"
  - Método "change_colors"
  - Método "rotate"
  - Método "shift"
  - Método "crop"
  - Método "filter"
  - Programa transform_simple.py
  - Programa transform_args.py
  - Programa transform_multi.py
  
- REQUISITOS OPCIONALES: 
  - Filtro avanzado luminosidad --> opcional1_brillo.py
  - Filtro avanzado tonos sepia --> opcional2_sepia.py
  - Filtro avanzado contraste --> opcional3_contraste.py
  - Programa interactivo --> opcional4_interactive.py
  - Detención de argumentos y parámetros incorrectos --> opcional5_transforms_errores.py, opcional5_detencion_errores.py
  
- COMENTARIOS: 
  - Ví lo de los commits cuando ya había estado trabajando en el proyecto varios días por lo que el primer commit recoge el trabajo de varios días. 
  - Mi vídeo debería tener una duración máxima de 3 minutos. He intentado recortar todo lo posible pero se me ha hecho imposible lograr que tenga esta duración por lo que es algo más largo. Espero que no sea un problema, gracias.  