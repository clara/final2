from transforms import mirror, blur, grayscale
import sys
import images as im


def get_filename(filename: str) -> str:
    name = ".".join(filename.split(".")[:-1]) + "_trans"
    ext = filename.split(".")[-1]
    new_filename = name + "." + ext

    return new_filename


def main():
    if len(sys.argv) < 3:
        print(f"\n[!] Uso: {sys.argv[0]} <imagen> <mirror, blur, grayscale>")
        sys.exit(1)
    else:
        filename = sys.argv[1]
        transforms = sys.argv[2:]

        pixels = im.read_img(filename)
        new_pixels = pixels

        for transform in transforms:
            if not (transform == "mirror" or transform == "blur" or transform == "grayscale"):
                print("\nUnknown transform!")
                print(f"\n[!]Uso: {sys.argv[0]} <imagen> <mirror, blur, grayscale>")
                sys.exit(1)

            else:
                if transform == "mirror":
                    new_pixels = mirror(new_pixels)
                elif transform == "blur":
                    new_pixels = blur(new_pixels)
                else:
                    new_pixels = grayscale(new_pixels)

        new_filename = get_filename(filename)
        im.write_img(new_pixels, new_filename)

        print("[+] Transformacion exitosa!")


if __name__ == '__main__':
    main()