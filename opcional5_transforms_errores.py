import images

class FilterError(Exception):
     pass
class CropError(Exception):
    pass
class CropDimensionError(Exception):
    pass
class ShiftError(Exception):
    pass
class RotateError(Exception):
    pass
class ChangeColorsError(Exception):
    pass


def mirror(image: dict) -> dict:
    width, height = images.size(image)
    new_image = images.create_blank(width, height)

    for i in range(width * height):
        x = i % width
        y = i // width
        new_image["pixels"][i] = image["pixels"][x + width * (height -1 -y)]
    #images.write_img(image=new_image, filename='imagen_mirror.png')
    return new_image


def grayscale(image: dict) -> dict:
    width, height = images.size(image)
    new_image = images.create_blank(width, height)

    for i in range(width * height):
        mean = (image['pixels'][i][0] + image['pixels'][i][1] + image['pixels'][i][2]) // 3
        new_image['pixels'][i] = (mean, mean, mean)
    #images.write_img(image=new_image, filename='imagen_greyscale.png')
    return new_image


def blur(image: dict) -> dict:
    width, height = images.size(image)
    new_image = images.create_blank(width, height)

    for i in range(width * height):
        x = i % width
        y = i // width
        ai = i - width
        bi = i - 1
        ci = i + 1
        di = i + width
        neighbours = [image['pixels'][i]]

        if not x == 0:
            neighbours.append(image['pixels'][ai])
        if not y == 0:
            neighbours.append(image['pixels'][bi])
        if not x == width - 1:
            neighbours.append(image['pixels'][ci])
        if not y == height - 1:
            neighbours.append(image['pixels'][di])
        valorr = []
        valorg = []
        valorb = []
        for n in neighbours:
            valorr.append(n[0])
            valorg.append(n[1])
            valorb.append(n[2])
        valor = (sum(valorr) // len(neighbours), sum(valorg) // len(neighbours), sum(valorb) // len(neighbours))
        new_image["pixels"][i] = valor

    #images.write_img(image=new_image, filename='imagen_blur.png')
    return new_image


def change_colors(image: dict, original: list[tuple[int, int, int]], change: list[tuple[int, int, int]]) -> dict:
    width, height = images.size(image)
    new_image = images.create_blank(width, height)

    original_int = []
    for i in original:
        if len(i) != 3:
            raise ChangeColorsError("Se necesitan tres valores RGB para los pixels de original")

        valor = list(i)
        try:
            valor[0] = int(valor[0])
            valor[1] = int(valor[1])
            valor[2] = int(valor[2])
            valor_tuple = tuple(valor)
            original_int.append(valor_tuple)
        except:
            raise ChangeColorsError("Los valores tienen que ser valores enteros")

    change_int = []
    for i in change:
        if len(i) != 3:
            raise ChangeColorsError("Se necesitan tres valores RGB para los pixels de change")

        valor = list(i)
        try:
            valor[0] = int(valor[0])
            valor[1] = int(valor[1])
            valor[2] = int(valor[2])
            valor_tuple = tuple(valor)
            change_int.append(valor_tuple)
        except:
            raise ChangeColorsError("los valores tienen que ser valores enteros")

    for i in range(width * height):
        if image['pixels'][i] in original_int:
            new_image['pixels'][i] = change_int[original_int.index(image['pixels'][i])]
        else:
            new_image['pixels'][i] = image['pixels'][i]
    #images.write_img(image=new_image, filename='imagen_change_colors.png')
    return new_image


def rotate(image: dict, direccion: str) -> dict:
    width, height = images.size(image)
    new_image = images.create_blank(height, width)
    if direccion not in ["right", "left"]:
        raise RotateError("Parámertos inválidos: Opciones: right, left")

    if direccion == 'right':
        for i in range(width * height):
            x = i % width
            y = i // width
            ir = height - 1 - y + x * height
            new_image['pixels'][ir] = image['pixels'][i]

    elif direccion == 'left':
        for i in range(width * height):
            x = i % width
            y = i // width
            ir = y + (width - 1 - x) * height
            new_image['pixels'][ir] = image['pixels'][i]

    else:
        print('Esta opción no es válida. Opciones: right o left')

    #images.write_img(image=new_image, filename='imagen_rotate.png')
    return new_image


def shift(image: dict, horizontal: int = 0, vertical: int = 0) -> dict:
    width, height = images.size(image)

    try:
        horizontal=int(horizontal)
        vertical=int(vertical)
    except:
        raise ShiftError("la funcion shift solo admite valores int")

    new_image = images.create_blank(width + horizontal, height + vertical)
    for i in range((width + horizontal) * (height + vertical)):
        xs = i % (width+horizontal)
        ys = i // (width+horizontal)
        x = xs-horizontal
        y = ys-vertical
        if xs<horizontal or ys<vertical:
            new_image['pixels'][i] = (0, 0, 0)
        else:
            ioriginal = x+width * y
            new_image['pixels'][i] = image['pixels'][ioriginal]

    #images.write_img(image=new_image, filename='imagen_shift.png')
    return new_image


def crop(image: dict, x: int, y: int, width: int, height: int) -> dict:
    try:
        x=int(x)
        y=int(y)
        width=int(width)
        height=int(height)
    except:
        raise CropError("la funcion crop solo admite valores int")

    original_width, original_height = images.size(image)
    new_image = images.create_blank(width, height)

    if x>original_width or y>original_height or x+width>original_width or y+height>original_height:
        raise CropDimensionError("las dimensiones introducidas son mayores a las de la imagen")

    for i in range(original_width * original_height):
        ox = i % original_width
        oy = i // original_width

        if x<=ox<x+width and y<=oy<y+height:
            icrop = ox - x + width * (oy - y)
            new_image['pixels'][icrop] = image['pixels'][i]

    #images.write_img(image=new_image, filename='imagen_crop.png')
    return new_image


def filter(image: dict, r: float, g: float, b: float) -> dict:
    width, height = images.size(image)
    new_image = images.create_blank(width, height)
    try:
        r=float(r)
        g=float(g)
        b=float(b)
    except:
        raise FilterError("la funcion filter solo admite valores float")

    for i in range(width * height):
        red = image['pixels'][i][0] * r
        if red > 255:
            red = 255

        green = image['pixels'][i][1] * g
        if green > 255:
            green = 255

        blue = image['pixels'][i][2] * b
        if blue > 255:
            blue = 255

        new_image['pixels'][i] = (round(red), round(green), round(blue))

    #images.write_img(image=new_image, filename='imagen_filter.png')
    return new_image