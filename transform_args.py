import sys
from transforms import *
from images import read_img, write_img

transform_n_arguments = {"mirror": 0,
                         "blur": 0,
                         "grayscale": 0,
                         "change_colors": 2,
                         "shift": 2,
                         "rotate": 1,
                         "filter": 3,
                         "crop": 4}


def get_filename(filename: str) -> str:
    name = ".".join(filename.split(".")[:-1]) + "_trans"
    ext = filename.split(".")[-1]
    new_filename = name + "." + ext

    return new_filename


def apply_transform(pixels, transforms):
    transform = transforms[0]
    args = transforms[1:]
    if transform_n_arguments[transform] == len(args):
        if transform == "mirror":
            new_pixels = mirror(pixels)
        elif transform == "blur":
            new_pixels = blur(pixels)
        elif transform == "grayscale":
            new_pixels = grayscale(pixels)
        elif transform == "change_colors":
            original = args[0].split(":")
            change = args[1].split(":")
            for j in range(len(original)):
                rgb_original = tuple(int(n) for n in original[j].split(","))
                rgb_change = tuple(int(n) for n in change[j].split(","))
                original[j] = rgb_original
                change[j] = rgb_change
            new_pixels = change_colors(pixels, original, change)
        elif transform == "shift":
            new_pixels = shift(pixels, int(args[0]), int(args[1]))
        elif transform == "rotate":
            new_pixels = rotate(pixels, args[0])
        elif transform == "filter":
            new_pixels = filter(pixels, float(args[0]), float(args[1]), float(args[2]))
        elif transform == "crop":
            new_pixels = crop(pixels, int(args[0]), int(args[1]), int(args[2]), int(args[3]))

        return new_pixels

    else:
        print("[!] Número incorrecto de argumentos")
        sys.exit(1)


def main():

    if len(sys.argv) < 3:
        print(f"\n[!] Uso: {sys.argv[0]} <imagen> <transforms>")
        sys.exit(1)
    else:
        filename = sys.argv[1]
        transforms = sys.argv[2:]

        transform = transforms[0]

        if transform not in transform_n_arguments:
            print("Transformacion incorrecta")
            sys.exit(1)

        pixels = read_img(filename)
        new_pixels = apply_transform(pixels, transforms)

        new_filename = get_filename(filename)
        write_img(new_pixels, new_filename)

        print("[+] Transformacion exitosa!")


if __name__ == '__main__':
    main()