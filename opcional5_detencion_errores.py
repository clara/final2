import sys
from opcional5_transforms_errores import *
from images import read_img, write_img

transform_n_arguments = {"mirror": 0,
                         "blur": 0,
                         "grayscale": 0,
                         "change_colors": 2,
                         "shift": 2,
                         "rotate": 1,
                         "filter": 3,
                         "crop": 4}

class NumeroParametrosIncorrectos(Exception):
    pass
class TransformacionIncorrecta(Exception):
    pass


def get_filename(filename: str) -> str:
    name = ".".join(filename.split(".")[:-1]) + "_trans"
    ext = filename.split(".")[-1]
    new_filename = name + "." + ext

    return new_filename


def apply_transform(pixels, transforms):
    transform = transforms[0]
    args = transforms[1:]
    if transform_n_arguments[transform] == len(args):
        if transform == "mirror":
            new_pixels = mirror(pixels)
        elif transform == "blur":
            new_pixels = blur(pixels)
        elif transform == "grayscale":
            new_pixels = grayscale(pixels)
        elif transform == "change_colors":
            original = args[0].split(":")
            change = args[1].split(":")
            if len(original) != len(change):
                raise ChangeColorsError("los valores original y change tiene  que tener la misma longitud")
            for j in range(len(original)):
                rgb_original = tuple(original[j].split(","))
                original[j] = rgb_original
            for j in range(len(original)):
                rgb_change = tuple(change[j].split(","))
                change[j] = rgb_change
            new_pixels = change_colors(pixels, original, change)
        elif transform == "shift":
            new_pixels = shift(pixels, args[0], args[1])
        elif transform == "rotate":
            new_pixels = rotate(pixels, args[0])
        elif transform == "filter":
            new_pixels = filter(pixels, args[0], args[1], args[2])
        elif transform == "crop":
            new_pixels = crop(pixels, args[0], args[1], args[2], args[3])

        return new_pixels

    else:
        print("[!] Número incorrecto de argumentos")
        sys.exit(1)


def main():
    try:
        if len(sys.argv) < 3:
            print(f"\n[!] Uso: {sys.argv[0]} <imagen> <transforms>")
            sys.exit(1)
        else:
            filename = sys.argv[1]
            transforms = sys.argv[2:]

            transform = transforms[0]

            if transform not in transform_n_arguments:
                raise TransformacionIncorrecta(f"Transformacion Incorrecta: {transform} no es una transformación válida")

            if transform_n_arguments[transform] != len(transforms) - 1:
                raise NumeroParametrosIncorrectos(f"Se ha introducido un número de parámetros incorrecto: la funcion {transform} requiere de {transform_n_arguments[transform]} argumentos y se han introcudido {len(transforms) - 1}")

            pixels = read_img(filename)
            new_pixels = apply_transform(pixels, transforms)

            new_filename = get_filename(filename)
            write_img(new_pixels, new_filename)

            print("[+] Transformacion exitosa!")
    except FileNotFoundError:
        print(f"La imagen {filename} no existe")
    except Exception as e:
        print(e)


if __name__ == '__main__':
    main()