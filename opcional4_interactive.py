import sys
from transforms import *
from images import read_img, write_img
from opcional2_sepia import sepia
from opcional1_brillo import brillo
from opcional3_contraste import contraste
from time import sleep

RESET = "\033[0m"
ROJO = "\033[91m"
VERDE = "\033[92m"
AMARILLO = "\033[93m"
AZUL = "\033[94m"
MAGENTA = "\033[95m"
CIAN = "\033[96m"
BLANCO = "\033[97m"


def print_color(mensaje, color):
    print(color + mensaje + RESET)


def print_menu_principal():
    print("\t 0. Salir del programa")
    print("\t 1. Guardar imagen en archivo")
    print("\t 2. Aplicar transformacion")


def print_menu_leer_imagen():
    print("\t 0. Salir del programa")
    print("\t 1. Leer imagen a transformar")


def print_menu_transformaciones():
    print_color("TRANSFORMACIONES: ", AMARILLO)
    print()
    print("1. Mirror")
    print("2. Blur")
    print("3. Grayscsale")
    print("4. Change colors")
    print("5. Shift")
    print("6. Rotate")
    print("7. Filter")
    print("8. Crop")
    print("9. Change contrast")
    print("10. Change brightness")
    print("11. Transform to sepia")

def print_menu_change_colors():
    print("1. Salir")
    print("2. Cambiar colores")


def guardar_imagen(pixels: dict):
    filename = input("Introduce el nombre del archivo al que quieres guardar la imagen: ")
    write_img(pixels, filename)


def leer_imagen():
    try:
        filename = input("Introduce el nombre de la imagen a transformar: ")
        pixels = read_img(filename)
        return pixels
    except FileNotFoundError:
        print_color("La imagen no existe! ", ROJO)
        sleep(2)
        leer_imagen()


def do_mirror(pixels: dict) -> dict:
    new_pixels = mirror(pixels)
    return new_pixels


def do_blur(pixels: dict) -> dict:
    new_pixels = blur(pixels)
    return new_pixels


def do_grayscale(pixels: dict) -> dict:
    new_pixels = grayscale(pixels)
    return new_pixels


def do_change_colors(pixels: dict) -> dict:
    print()
    original = []
    change = []
    print_menu_change_colors()
    try:
        opcion = int(input("Introduce una opción: "))
        while opcion != 1:
            print_color("Introduce los valores del color que quieres cambiar: ", AMARILLO)
            r1 = int(input("introduce el valor R: "))
            g1 = int(input("introduce el valor G: "))
            b1 = int(input("introduce el valor B: "))

            print_color("Introduce los valores del color al que quieres cambiar: ", AMARILLO)
            r2 = int(input("introduce el valor R: "))
            g2 = int(input("introduce el valor G: "))
            b2 = int(input("introduce el valor B: "))

            original.append((r1, g1, b1))
            change.append((r2, g2, b2))

            print_menu_change_colors()
            opcion = int(input("Introduce una opción: "))

    except ValueError:
        print_color("Introduce un entero dentro del rango posible! ", ROJO)

    new_pixels = change_colors(pixels, original, change)
    return new_pixels


def do_shift(pixels: dict) -> dict:
    print()
    desp_hor = int(input("introduce el desplazamiento horizontal: "))
    desp_ver = int(input("introduce el desplazamiento vertical: "))

    new_pixels = shift(pixels, desp_hor, desp_ver)
    return new_pixels


def do_rotate(pixels: dict) -> dict:
    print()
    direccion = input("introduce la rotacion deseada (opciones: left/right): ")

    new_pixels = rotate(pixels, direccion)
    return new_pixels


def do_filter(pixels: dict) -> dict:
    print()
    r = float(input("Introduce el multiplicador para R: "))
    g = float(input("Introduce el multiplicador para G: "))
    b = float(input("Introduce el multiplicador para B: "))

    new_pixels = filter(pixels, r, g, b)
    return new_pixels


def do_crop(pixels: dict) -> dict:
    print()
    print_color("Introduce los valores del rectángulo a recortar: ", AMARILLO)
    x = int(input("Introduce la COLUMNA de la esquina superior izquierda: "))
    y = int(input("Introduce la FILA de la esquina superior izquierda: "))
    width = int(input("Introduce la ANCHURA: "))
    height = int(input("Introduce la ALTURA: "))

    new_pixels = crop(pixels, x, y, width, height)
    return new_pixels


def do_brillo(pixels: dict) -> dict:
    print()
    change = float(input("introduce el  multiplicador (>1 para aumentar el brillo, <1 para disminuirlo): "))

    new_pixels = brillo(pixels, change)
    return new_pixels


def do_sepia(pixels: dict) -> dict:
    new_pixels = sepia(pixels)
    return new_pixels


def do_contraste(pixels: dict) -> dict:
    cambio_contraste = int(input("introduce el cambio de contraste deseado"))
    new_pixels = contraste(pixels, cambio_contraste)
    return new_pixels


def aplicar_transformacion(pixels: dict) -> dict:
    print_menu_transformaciones()

    try:
        opcion = int(input("Introduce una transformacion: "))

        if opcion == 1:
            new_pixels = do_mirror(pixels)
        elif opcion == 2:
            new_pixels = do_blur(pixels)
        elif opcion == 3:
            new_pixels = do_grayscale(pixels)
        elif opcion == 4:
            new_pixels = do_change_colors(pixels)
        elif opcion == 5:
            new_pixels = do_shift(pixels)
        elif opcion == 6:
            new_pixels = do_rotate(pixels)
        elif opcion == 7:
            new_pixels = do_filter(pixels)
        elif opcion == 8:
            new_pixels = do_crop(pixels)
        elif opcion == 9:
            new_pixels = do_contraste(pixels)
        elif opcion == 10:
            new_pixels = do_brillo(pixels)
        elif opcion == 11:
            new_pixels = do_sepia(pixels)

        return new_pixels

    except ValueError:
        print_color("No has introducido bien el número! ", ROJO)
        aplicar_transformacion(pixels)
    except UnboundLocalError:
        print_color("Opción inválida! ", ROJO)
        sleep(2)
        aplicar_transformacion(pixels)


def main():

    SALIR = 0
    opcion = -1
    pixels = []
    new_pixels = []

    while opcion != SALIR:
        if pixels != []: #si la imagen se ha leido
            print_menu_principal()
            try:
                opcion = int(input("Introduce una opción: "))
            except:
                print_color("Introduce un entero! ", ROJO)

            try:
                if opcion == 1:
                    guardar_imagen(new_pixels)
                elif opcion == 2:
                    new_pixels = aplicar_transformacion(new_pixels)
            except ValueError:
                print_color("Valor incorrecto! ", ROJO)

        else:
            print_menu_leer_imagen()
            try:
                opcion = int(input("Introduce una opción: "))
                if opcion == 1:
                    pixels = leer_imagen()
                    new_pixels = pixels
            except ValueError:
                print_color("Introduce un entero! ", ROJO)


if __name__ == '__main__':
    main()