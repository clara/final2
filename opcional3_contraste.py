import images as im


def contraste(image: dict, contraste: float) -> dict:
    width, height = im.size(image)
    new_image = im.create_blank(width, height)
    c = (100.0 + contraste) / 100.0
    c = c**2  # c al cuadrado

    for i in range(width * height):
        pixel = image['pixels'][i]
        original_r = pixel[0]
        original_g = pixel[1]
        original_b = pixel[2]
        contraste_r = round((((original_r / 255 - 0.5) * c) + 0.5) * 255)
        contraste_g = round((((original_g / 255 - 0.5) * c) + 0.5) * 255)
        contraste_b = round((((original_b / 255 - 0.5) * c) + 0.5) * 255)

        pixel_contraste = (contraste_r, contraste_g, contraste_b)
        pixel_contraste_lista = list(pixel_contraste)
        for x in range(len(pixel_contraste)):
            if pixel_contraste_lista[x] < 0:
                pixel_contraste_lista[x] = 0
            elif pixel_contraste_lista[x] > 255:
                pixel_contraste_lista[x] = 255
        new_image['pixels'][i] = pixel_contraste

    im.write_img(image=new_image, filename='imagen_contraste.png')
    return new_image


pixels = im.read_img("cafe.jpg")
contraste(pixels, 50)
